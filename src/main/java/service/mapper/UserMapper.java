package service.mapper;

import entity.User;
import org.mapstruct.Mapper;
import service.dto.UserDTO;

import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface UserMapper extends EntityMapper<UserDTO, User> {

    UserDTO toDto(User user);

    User toEntity(UserDTO userDTO);

    List<UserDTO> toDtos(List<User> users);

    List<User> toEntities(List<UserDTO> userDTOS);
}
