package service;

import entity.User;
import repository.UserRepository;
import service.dto.UserDTO;
import service.mapper.UserMapper;

import javax.persistence.EntityNotFoundException;
import java.util.List;

public class UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    public UserService(final UserRepository userRepository, final UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }

    public List<UserDTO> getAllUsers() {
        List<User> userList = userRepository.findAll();

        return userMapper.toDtos(userList);
    }

    public UserDTO getUser(Long userId) {
        User byId = userRepository.findById(userId).orElseThrow(() -> new EntityNotFoundException(
                String.format("User not found by id: %s", userId)));
        return userMapper.toDto(byId);
    }

    public UserDTO createUser(final UserDTO userDTO) {
        User user = new User();
        user = setUserData(userDTO, user);
        user.setEmail(userDTO.getEmail());

        user = userRepository.saveAndFlush(user);
        return userMapper.toDto(user);
    }

    public UserDTO updateUser(final UserDTO userDTO) {
        User user = userRepository.findUserByEmail(userDTO.getEmail());

        return userMapper.toDto(setUserData(userDTO, user));
    }

    public void deleteUser(final UserDTO userDTO) {
        User user = userRepository.findUserByEmail(userDTO.getEmail());
        userRepository.deleteById(user.getId());
    }


    private User setUserData(UserDTO userDto, User user) {
        user.setName(userDto.getName());
        user.setPassword(userDto.getPassword());
        user.setLastLoginDate(userDto.getLastLoginDate());

        return user;
    }

}
