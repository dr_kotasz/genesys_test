package controller;

import org.springframework.web.bind.annotation.*;
import service.UserService;
import service.dto.UserDTO;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/")
public class UserController {

  private final UserService userService;

  public UserController(final UserService userService) {
    this.userService = userService;
  }

  @GetMapping("/users")
  public List<UserDTO> getAllUsers() {
    return userService.getAllUsers();
  }

  @GetMapping("/users/{userId}")
  public UserDTO getUser(@PathVariable Long userId) {
    return userService.getUser(userId);
  }

  @PostMapping("/users/new")
  public UserDTO createUser(@Valid @RequestBody final UserDTO userDTO) {
    return userService.createUser(userDTO);
  }

  @PutMapping("/users/{userId}")
  public UserDTO updateUser(@Valid @RequestBody final UserDTO userDTO) {
    return userService.updateUser(userDTO);
  }

  @DeleteMapping("/users/{userId}")
  public void deleteUser(@Valid @RequestBody final UserDTO userDTO) {
    userService.deleteUser(userDTO);
  }
}
